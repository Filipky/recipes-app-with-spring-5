package com.example.recipeapp.repositories;

import com.example.recipeapp.domain.Ingredient;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Felipe Díaz on 26/01/2018.
 */
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {
}
