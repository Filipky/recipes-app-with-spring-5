package com.example.recipeapp.repositories;

import com.example.recipeapp.domain.Recipe;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Felipe Díaz on 17/01/2018.
 */
public interface RecipeRepository extends CrudRepository<Recipe, Long>{
}
