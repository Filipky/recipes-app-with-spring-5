package com.example.recipeapp.services;

import com.example.recipeapp.commands.IngredientCommand;
import org.springframework.stereotype.Service;

/**
 * Created by Felipe Díaz on 25/01/2018.
 */

@Service
public interface IngredientService {

    IngredientCommand findByRecipeIdAndIngredientId(Long recipeId, Long ingredientId);

    IngredientCommand saveIngredientCommand(IngredientCommand ingredientCommand);

    void deleteById(Long recipeId, Long ingredientId);

}