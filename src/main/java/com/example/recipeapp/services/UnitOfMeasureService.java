package com.example.recipeapp.services;

import com.example.recipeapp.commands.UnitOfMeasureCommand;
import com.example.recipeapp.domain.UnitOfMeasure;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Created by Felipe Díaz on 25/01/2018.
 */
@Service
public interface UnitOfMeasureService {

    public Set<UnitOfMeasureCommand> listAllUoms();
}
