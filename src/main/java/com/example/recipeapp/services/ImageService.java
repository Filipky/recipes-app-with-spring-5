package com.example.recipeapp.services;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Felipe Díaz on 9/02/2018.
 */
@Service
public interface ImageService {

    void  saveImageFile(Long recipeId, MultipartFile multipartFile);
}
