package com.example.recipeapp.domain;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Felipe Díaz on 17/01/2018.
 */
@Data
@Entity
public class UnitOfMeasure {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String uom;
    private String description;

}
