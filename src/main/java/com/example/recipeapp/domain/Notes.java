package com.example.recipeapp.domain;

import lombok.*;

import javax.persistence.*;

/**
 * Created by USER on 17/01/2018.
 */
@Data
@Entity
@EqualsAndHashCode(exclude = {"recipe"})
public class Notes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Recipe recipe;

    @Lob
    private String recipeNotes;

}
