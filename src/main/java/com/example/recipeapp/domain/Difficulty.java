package com.example.recipeapp.domain;

/**
 * Created by Felipe Díaz on 17/01/2018.
 */
public enum Difficulty {
    EASY, MEDIUM, HARD, MODERATE
}
